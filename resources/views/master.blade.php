<!DOCTYPE html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <a href="{{ route('logout_session') }}">Logout</a>

            <main class="py-4">
                @yield('content')
            </main>
        </div>
    </body>
    </html>