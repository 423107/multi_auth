<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Superadmin extends Controller
{

    public function index(Request $request){
    	return view('superadmin');
    }
}
