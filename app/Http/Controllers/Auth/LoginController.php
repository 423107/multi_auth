<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:superadmin')->except('logout');
        $this->middleware('guest:marketing')->except('logout');
        $this->middleware('guest:support')->except('logout');
        $this->middleware('guest:customer')->except('logout');
           
    }



    public function showSuperadminLoginForm()
    {
        return view('auth.login', ['url' => 'superadmin']);
    }

    public function superadminLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('superadmin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect('superadmin');
        }
        return back()->withInput($request->only('email', 'remember'));
    }


    public function showMarketingLoginForm()
    {
        return view('auth.login', ['url' => 'marketing']);
    }

    public function marketingLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('marketing')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect('marketing');
        }
        return back()->withInput($request->only('email', 'remember'));
    }


    public function showSupportLoginForm()
    {
        return view('auth.login', ['url' => 'support']);
    }

    public function supportLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('support')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect('support');
        }
        return back()->withInput($request->only('email', 'remember'));
    }


    public function showCustomerLoginForm()
    {
        return view('auth.login', ['url' => 'customer']);
    }

    public function customerLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect('customer');
        }
        return back()->withInput($request->only('email', 'remember'));
    }

    public function logout(Request $request) {
        Auth::logout();
        Auth::guard('superadmin')->logout();
        Auth::guard('marketing')->logout();
        Auth::guard('support')->logout();
        Auth::guard('customer')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/login/superadmin');
    }
}
