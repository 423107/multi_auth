<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        
        switch ($guard) {
            case 'superadmin':
                if (Auth::guard($guard)->check()) {
                    return $next($request);
                }
                break;
            case 'marketing':
                if (Auth::guard($guard)->check()) {
                    return $next($request);
                }
                break;
            case 'support':
                if (Auth::guard($guard)->check()) {
                    return $next($request);
                }
                break;
            case 'customer':
                if (Auth::guard($guard)->check()) {
                    return $next($request);
                }
                break;
            default:
                if (Auth::guard($guard)->check()) {
                    return redirect('login');
                }
                break;
      }

        return $next($request);
    }
}
