<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login/superadmin');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('logout','Auth\LoginController@logout')->name('logout_session');

Route::get('/login/superadmin', 'Auth\LoginController@showSuperadminLoginForm');
Route::get('/login/marketing', 'Auth\LoginController@showMarketingLoginForm');
Route::get('/login/support', 'Auth\LoginController@showSupportLoginForm');
Route::get('/login/customer', 'Auth\LoginController@showCustomerLoginForm');

Route::post('/login/superadmin', 'Auth\LoginController@superadminLogin');
Route::post('/login/marketing', 'Auth\LoginController@marketingLogin');
Route::post('/login/support', 'Auth\LoginController@supportLogin');
Route::post('/login/customer', 'Auth\LoginController@customerLogin');

Route::view('/home', 'home')->middleware('auth');


Route::group(['middleware' => 'auth:superadmin'], function () {
    Route::get('/superadmin', 'Superadmin@index');
});

Route::group(['middleware' => 'auth:marketing'], function () {
	Route::get('/marketing', 'Marketing@index');
});


Route::group(['middleware' => 'auth:support'], function () {
	Route::get('/support', 'Support@index');
});


Route::group(['middleware' => 'auth:customer'], function () {
	Route::get('/customer', 'Customer@index');
});

