<?php
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(SuperadminTableSeeder::class);
         $this->call(MarketingTableSeeder::class);
         $this->call(SupportTableSeeder::class);
         $this->call(CustomerTableSeeder::class);
         $this->call(UsersTableSeeder::class);
    }
}