<?php

use Illuminate\Database\Seeder;

class MarketingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marketing')->insert([
         'email' => 'marketing@gmail.com',
         'password' => Hash::make('123123'),
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
